package com.biblezon.bibleorder.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class GameManager {
	// Manage game data here

	private static String PREF_NAME = "game_manager";
	private SharedPreferences preferences;
	private SharedPreferences.Editor editor;
	@SuppressWarnings("unused")
	private Context context;

	public GameManager(Context context) {
		this.context = context;
		preferences = context.getSharedPreferences(PREF_NAME,
				Context.MODE_PRIVATE);
		editor = preferences.edit();
	}

	public void setGameTopScore(int score) {
		if (getGameTopScore() == 0) {
			editor.putInt("score", score).commit();
		} else {
			if (score <= getGameTopScore()) {
				editor.putInt("score", score).commit();
			}
		}

	}

	public int getGameTopScore() {
		return preferences.getInt("score", 0);
	}

}
