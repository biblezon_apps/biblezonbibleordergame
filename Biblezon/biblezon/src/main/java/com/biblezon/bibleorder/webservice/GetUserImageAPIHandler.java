package com.biblezon.bibleorder.webservice;

import android.app.Activity;
import android.graphics.Bitmap;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.biblezon.bibleorder.application.AppApplicationController;
import com.biblezon.bibleorder.ihelper.GlobalKeys;
import com.biblezon.bibleorder.utils.AppDefaultUtils;
import com.biblezon.bibleorder.utils.AppHelper;
import com.biblezon.bibleorder.webservice.control.WebAPIResponseListener;

/**
 * Get user image API Handler
 * 
 * @author Shruti
 * 
 */
public class GetUserImageAPIHandler {
	/**
	 * Instance object of get image API
	 */
	@SuppressWarnings("unused")
	private Activity mActivity;
	/**
	 * Debug TAG
	 */
	private String TAG = GetUserImageAPIHandler.class.getSimpleName();
	/**
	 * Request Data
	 */
	private String image_path;
	/**
	 * API Response Listener
	 */
	private WebAPIResponseListener mResponseListener;
	int position;

	/**
	 * 
	 * @param mActivity
	 * @param password
	 * @param email
	 * @param webAPIResponseListener
	 */
	public GetUserImageAPIHandler(Activity mActivity, String image_path,
			WebAPIResponseListener webAPIResponseListener,
			boolean isProgreesShowing, int position) {

		this.mActivity = mActivity;
		this.image_path = image_path;
		this.mResponseListener = webAPIResponseListener;
		this.position = position;
		postAPICall();
	}

	/**
	 * Call API to Get User image
	 */
	private void postAPICall() {
		@SuppressWarnings("deprecation")
		ImageRequest mImageRequest = new ImageRequest(image_path.trim(),
				new Listener<Bitmap>() {

					@Override
					public void onResponse(Bitmap bitmap) {
						if (bitmap != null) {
							AppDefaultUtils.showLog(TAG,
									"Success To get user image");
							mResponseListener.onSuccessOfResponse(bitmap);
						} else {
							AppDefaultUtils.showLog(TAG, "bitmap is null");
							// mResponseListener.onFailOfResponse(mActivity
							// .getResources().getString(R.string.RETRY));
							mResponseListener.onFailOfResponse("");
						}
					}
				}, 0, 0, null, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						AppDefaultUtils.showLog(TAG,
								"get image onErrorResponse " + error);
						mResponseListener.onFailOfResponse("");
					}
				}) {

		};
		// Adding request to request queue
		AppApplicationController.getInstance().addToImageRequestQueue(
				mImageRequest, GlobalKeys.GET_IMAGE_KEY + position);
		// set request time-out
		mImageRequest.setRetryPolicy(new DefaultRetryPolicy(
				AppHelper.ONE_SECOND_TIME * AppHelper.API_REQUEST_TIME,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		// Canceling request
		// ReDriverApplicationController.getInstance().getRequestQueue()
		// .cancelAll(GlobalKeys.LOGIN_REQUEST_KEY);
	}
}