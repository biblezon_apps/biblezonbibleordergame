package com.biblezon.bibleorder.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.biblezon.bibleorder.R;
import com.biblezon.bibleorder.model.SpellWordModel;

/**
 * show empty spaces for puzzle words on GUI
 * 
 * @author Shruti
 * 
 */
public class SpellWordAdapter extends BaseAdapter {
	/**
	 * list of letters for puzzle words
	 */
	private List<SpellWordModel> mWordList;
	/**
	 * Context object
	 */
	private Context context;
	@SuppressWarnings("unused")
	private Activity activity;
	@SuppressWarnings("unused")
	private LayoutInflater mLayoutInflater;

	/**
	 * Debugging TAG
	 */
	private String TAG = SpellWordAdapter.class.getSimpleName();
	/**
	 * zoom in and zoom out animation
	 */
	Animation zoom_in, zoom_out;

	/**
	 * Basic constructor of class
	 * 
	 * @param activity
	 */
	public SpellWordAdapter(Activity activity) {
		this.context = activity;
		this.activity = activity;
		// zoom_in = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
		// zoom_out = AnimationUtils.loadAnimation(context, R.anim.zoom_out);
		try {
			mLayoutInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			mWordList = new ArrayList<SpellWordModel>();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Add updated data on List
	 */
	public void addUpdateDataIntoList(List<SpellWordModel> mData) {
		mWordList = new ArrayList<SpellWordModel>();
		mWordList = mData;
	}

	@Override
	public int getCount() {
		if (mWordList != null) {
			return mWordList.size();
		} else {
			return 0;
		}

	}

	@Override
	public SpellWordModel getItem(int position) {
		if (mWordList != null) {
			return mWordList.get(position);
		} else {
			return null;
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = View.inflate(context.getApplicationContext(),
					R.layout.empty_box, null);
			new ViewHolder(convertView);
		}
		ViewHolder holder = (ViewHolder) convertView.getTag();
		if (mWordList != null) {
			SpellWordModel letter_model = mWordList.get(position);
			if (letter_model != null) {
				// AppDefaultUtils.showErrorLog(TAG,
				// "LATTER :" + letter_model.getLetter());

				holder.random_alphabet.setText(letter_model.getLetter());
			}
		}
		return convertView;
	}

	/**
	 * List view row object and its views
	 * 
	 * @author Shruti
	 * 
	 */
	class ViewHolder {
		TextView random_alphabet;

		public ViewHolder(View view) {
			random_alphabet = (TextView) view.findViewById(R.id.alphabet);
			view.setTag(this);
		}
	}
}
