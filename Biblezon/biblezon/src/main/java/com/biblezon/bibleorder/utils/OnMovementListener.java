package com.biblezon.bibleorder.utils;

public interface OnMovementListener {
	public abstract void onMovement();
}
