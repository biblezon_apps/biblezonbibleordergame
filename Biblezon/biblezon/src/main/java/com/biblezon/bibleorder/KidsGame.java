package com.biblezon.bibleorder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.biblezon.bibleorder.utils.DraggableGridView;
import com.biblezon.bibleorder.utils.GameManager;
import com.biblezon.bibleorder.utils.NameList;
import com.biblezon.bibleorder.utils.OnMovementListener;
import com.biblezon.bibleorder.utils.OnRearrangeListener;
import com.plattysoft.leonids.ParticleSystem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

@SuppressLint("NewApi")
public class KidsGame extends Activity {

    public static String GAME_MODE = "off";
    private boolean gameMode = false;
    public static int movement = 0;
    int winCoundown;
    private ParticleSystem ps;

    String whichGame;
    TextView mvTxt;
    DraggableGridView dgv;
    ArrayList<String> gameList;
    ArrayList<String> randomList;
    ArrayList<String> game = new ArrayList<String>();
    HashMap<String, GameData> gameDataMap;

    LayoutInflater inflater;

    @Override
    protected void onResume() {
        super.onResume();
        updateMove(movement);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            gameMode = savedInstanceState.getBoolean(GAME_MODE);
        }

        whichGame = getIntent().getExtras().getString("whichgame", "game1");
        inflater = LayoutInflater.from(KidsGame.this);

        setContentView(R.layout.kidsgame);
        dgv = ((DraggableGridView) findViewById(R.id.vgv));
        mvTxt = (TextView) findViewById(R.id.move_txt);
        mvTxt.setTypeface(Typeface.createFromAsset(getAssets(), "comicbd.ttf"));

        dgv.setOnRearrangeListener(new OnRearrangeListener() {
            public void onRearrange(int oldIndex, int newIndex) {
                String word = game.remove(oldIndex);
                if (oldIndex < newIndex) {
                    game.add(newIndex, word);
                } else {
                    game.add(newIndex, word);
                }
            }
        });

        dgv.setOnMovementListener(new OnMovementListener() {
            @Override
            public void onMovement() {
                // Count Movement here
                updateMove(++movement);
                dgv.updateGameData(game);
            }
        });

        if (!gameMode) {
            setGame(whichGame);
        } else {
            continueGame(KidsPuzzleApp.whichGame);
        }

    }

    private void setGame(String whichGame) {
        gameMode = true;
        gameDataMap = NameList.generateGameData(whichGame);
        gameList = NameList.getGameList(whichGame);
        randomList = NameList.getGameList(whichGame);
        Collections.shuffle(randomList);

        for (int i = 0; i < randomList.size(); i++) {
            String word = randomList.get(i);
            ImageView image = new ImageView(KidsGame.this);
            image.setImageBitmap(getTileView(word));
            dgv.addView(image);
            game.add(word);
        }

        dgv.setCurrentGameData(gameDataMap, game);
        onResume();
    }

    @SuppressLint("InflateParams")
    private Bitmap getTileView(String s) {

        View mView = inflater.inflate(R.layout.tile, null);
        TextView txtView = (TextView) mView.findViewById(R.id.tile);
        txtView.setText(s);
        txtView.setTypeface(Typeface
                .createFromAsset(getAssets(), "comicbd.ttf"));

        mView.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        Bitmap returnedBitmap = Bitmap.createBitmap(mView.getMeasuredWidth(),
                mView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        mView.layout(0, 0, mView.getMeasuredWidth(), mView.getMeasuredHeight());
        mView.draw(canvas);

        return returnedBitmap;
    }

    private void updateMove(int move) {
        mvTxt.setText("Move - " + move);
        playMovementSound();
        checkWin();
    }

    private void checkWin() {
        winCoundown = 0;
        for (int i = 0; i < gameList.size(); i++) {
            if (!gameList.get(i).equalsIgnoreCase(game.get(i))) {
                break;
            } else {
                ++winCoundown;
            }
        }

        // Toast.makeText(KidsGame.this, ""+winCoundown,
        // Toast.LENGTH_SHORT).show();
        if ((gameList.size()) == winCoundown) {
            // You Win
            openWinDialog();
        }
    }

    private void openWinDialog() {

        new GameManager(KidsGame.this).setGameTopScore(movement);

        // play win sound
        playWinSound();

        AlertDialog.Builder dialog = new AlertDialog.Builder(KidsGame.this);
        dialog.setCancelable(false);
        dialog.setMessage("Yahoo !!! you have solve the puzzle in " + movement
                + " Move");
        dialog.setPositiveButton("Restart", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();
                reset();
                setGame(whichGame);
            }
        });

        dialog.setNegativeButton("Exit", new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int arg1) {
                reset();
                dialog.dismiss();
                finish();
            }
        });

        dialog.create();
        dialog.show();
    }

    private void reset() {

        dgv.removeAllViews();

        game.clear();
        randomList.clear();
        gameList.clear();
        movement = 0;
        winCoundown = 0;
    }

    private void playMovementSound() {
        MediaPlayer player = MediaPlayer.create(KidsGame.this,
                R.raw.button_splash);
        player.start();
    }

    private void playWinSound() {
        MediaPlayer player = MediaPlayer.create(KidsGame.this, R.raw.ending2);
        player.start();

        Display display = getWindowManager().getDefaultDisplay();
        final Point point = new Point();
        display.getSize(point);
        int x = point.x / 2;
        int y = point.y / 2;

        // Create a particle system and start emiting
        ps = new ParticleSystem(KidsGame.this, 100, R.drawable.star_pink, 10000);
        ps.setScaleRange(0.7f, 1.3f);
        ps.setSpeedRange(0.05f, 0.1f);
        ps.setRotationSpeedRange(90, 180);
        ps.setFadeOut(200, new AccelerateInterpolator());
        ps.emit(x, y, 40);
    }

    @Override
    public void onBackPressed() {
        //
        // if (movement >= 1) {
        // showExitDialog();
        // } else {
        super.onBackPressed();
        // }
    }

    @SuppressWarnings("unused")
    private void showExitDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(KidsGame.this);
        dialog.setMessage("DO you want to quit the game !!!");
        dialog.setCancelable(false);
        dialog.setPositiveButton("YES", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();
                reset();
                finish();
            }
        });

        dialog.setNegativeButton("NO", new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();
            }
        });

        dialog.create();
        dialog.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        gameMode = false;

        // saveData
        KidsPuzzleApp.saveData(whichGame, movement, game);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(GAME_MODE, gameMode);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    private void continueGame(String whichGame) {

        gameMode = true;
        movement = KidsPuzzleApp.movement;
        gameDataMap = NameList.generateGameData(whichGame);
        gameList = NameList.getGameList(whichGame);
        randomList = KidsPuzzleApp.getGameList();

        for (int i = 0; i < randomList.size(); i++) {
            String word = randomList.get(i);
            ImageView image = new ImageView(KidsGame.this);
            image.setImageBitmap(getTileView(word));
            dgv.addView(image);
            game.add(word);
        }

        dgv.setCurrentGameData(gameDataMap, game);
        onResume();
    }

}
